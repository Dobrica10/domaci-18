<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="shortcut icon" type="image/x-icon" href="pic/poodle.png" />
  <title>Konverzija</title>
</head>
<body>
<?php include('db.php'); ?>
<form action="convert.php" method="POST" enctype="multipart/form-data" id="form">
<fieldset >
  <legend>Konverzija</legend>
    <label>Iznos</label><br>
    <input type="text" name="iznos" value="" required="required"><br><br>
    <label>Valuta</label><br>
    <select type="text" name="valuta"  value="" required="required">
      <option value="">- Choose -</option>";
      <?php

      $sql = "SELECT * FROM valuta ORDER BY naziv ASC";
      $result = mysqli_query($connection,$sql) or die(mysql_error());

      if (mysqli_num_rows($result)>0) {
        
        while ($record = mysqli_fetch_array($result,MYSQLI_BOTH)){
          echo "<option value=\"$record[id]\">$record[naziv]</option>";
         
        }
      }

      ?>
      
    </select><br><br>
  <label for="kupK">
  <input type="radio" name="kurs" value="kupovni" id="kupK" checked> Kupovni<br>
  </label>
  <label for="sredK">
  <input type="radio" name="kurs" value="srednji" id="sredK"> Srednji<br>
  </label>  
  <label for="prodK">
  <input type="radio" name="kurs" value="prodajni" id="prodK"> Prodajni
  </label>
  <br><br>
    <input type="submit" name="convert" id="convert" value="convert">

  </fieldset>
</form>

<a href="create_json_file.php" id="link">Link do JSON-a</a>

</body>
</html>