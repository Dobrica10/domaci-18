-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 15, 2018 at 10:22 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kursna_lista`
--

-- --------------------------------------------------------

--
-- Table structure for table `konvertovana_valuta`
--

DROP TABLE IF EXISTS `konvertovana_valuta`;
CREATE TABLE IF NOT EXISTS `konvertovana_valuta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `iznos` float NOT NULL,
  `valuta` varchar(20) CHARACTER SET utf8 NOT NULL,
  `iznos_kursa` float NOT NULL,
  `konvertovani_iznos` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konvertovana_valuta`
--

INSERT INTO `konvertovana_valuta` (`id`, `datum`, `iznos`, `valuta`, `iznos_kursa`, `konvertovani_iznos`) VALUES
(2, '2018-06-15', 22, 'Danska kruna', 15.8099, 347.818),
(3, '2018-06-15', 22, 'Danska kruna', 15.8099, 347.818),
(4, '2018-06-14', 789, 'Norveška kruna', 12.5306, 9886.64),
(5, '2018-06-14', 58, 'Australijski dolar', 75.6849, 4389.72),
(6, '2018-06-15', 58, 'Britanska funta', 133.619, 7749.9),
(7, '2018-06-15', 25, 'Australijski dolar', 75.4578, 1886.44),
(8, '2018-06-15', 99, 'Rublja', 1.5978, 158.182);

-- --------------------------------------------------------

--
-- Table structure for table `kurs_dinara`
--

DROP TABLE IF EXISTS `kurs_dinara`;
CREATE TABLE IF NOT EXISTS `kurs_dinara` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `valuta` int(5) NOT NULL,
  `kupovni` float NOT NULL,
  `prodajni` float NOT NULL,
  `srednji` float NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kurs_dinara`
--

INSERT INTO `kurs_dinara` (`ID`, `datum`, `valuta`, `kupovni`, `prodajni`, `srednji`) VALUES
(1, '2018-06-14', 1, 117.812, 118.521, 118.167),
(2, '2018-06-14', 2, 99.7648, 100.365, 100.065),
(3, '2018-06-14', 3, 101.257, 101.866, 101.562),
(4, '2018-06-14', 4, 133.619, 134.424, 134.021),
(5, '2018-06-14', 5, 75.4578, 75.912, 75.6849),
(6, '2018-06-14', 6, 76.906, 77.3688, 77.1374),
(7, '2018-06-14', 7, 11.5908, 11.6606, 11.6257),
(8, '2018-06-14', 8, 15.8099, 15.9051, 15.8575),
(9, '2018-06-14', 9, 12.4556, 12.5306, 12.4931),
(10, '2018-06-14', 10, 0.905691, 0.911141, 0.908416),
(11, '2018-06-14', 11, 1.593, 1.6026, 1.5978),
(12, '2018-06-14', 0, 15.5958, 15.6896, 15.6427);

-- --------------------------------------------------------

--
-- Table structure for table `valuta`
--

DROP TABLE IF EXISTS `valuta`;
CREATE TABLE IF NOT EXISTS `valuta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(20) CHARACTER SET utf8 NOT NULL,
  `kod` varchar(5) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `valuta`
--

INSERT INTO `valuta` (`id`, `naziv`, `kod`) VALUES
(1, 'euro', 'eur'),
(2, 'Američki dolar', 'usd'),
(3, 'Švajcarski franak', 'chf'),
(4, 'Britanska funta', 'gbp'),
(5, 'Australijski dolar', 'aud'),
(6, 'Kanadski dolar', 'cad'),
(7, 'Švedska kruna', 'sek'),
(8, 'Danska kruna', 'dkk'),
(9, 'Norveška kruna', 'nok'),
(10, 'Japanski jen', 'jpy'),
(11, 'Rublja', 'rub');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
